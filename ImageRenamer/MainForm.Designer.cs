﻿namespace FilesRenamer
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.buttonRename = new System.Windows.Forms.Button();
            this.textBoxPath = new System.Windows.Forms.TextBox();
            this.openFileDialogImage = new System.Windows.Forms.OpenFileDialog();
            this.buttonFile = new System.Windows.Forms.Button();
            this.buttonFolder = new System.Windows.Forms.Button();
            this.folderBrowserDialogImage = new System.Windows.Forms.FolderBrowserDialog();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripArrangeButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.textBoxDestination = new System.Windows.Forms.TextBox();
            this.buttonOutput = new System.Windows.Forms.Button();
            this.fontDialog1 = new System.Windows.Forms.FontDialog();
            this.groupBoxSource = new System.Windows.Forms.GroupBox();
            this.groupBoxDestination = new System.Windows.Forms.GroupBox();
            this.checkBoxSaveDupsForTheEnd = new System.Windows.Forms.CheckBox();
            this.detailedProgressBar1 = new FilesRenamer.DetailedProgressBar();
            this.toolStrip1.SuspendLayout();
            this.groupBoxSource.SuspendLayout();
            this.groupBoxDestination.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonRename
            // 
            this.buttonRename.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.buttonRename.Location = new System.Drawing.Point(18, 191);
            this.buttonRename.Name = "buttonRename";
            this.buttonRename.Size = new System.Drawing.Size(186, 23);
            this.buttonRename.TabIndex = 0;
            this.buttonRename.Text = "Rename";
            this.buttonRename.UseVisualStyleBackColor = false;
            this.buttonRename.Click += new System.EventHandler(this.buttonRename_Click);
            // 
            // textBoxPath
            // 
            this.textBoxPath.Location = new System.Drawing.Point(6, 19);
            this.textBoxPath.Name = "textBoxPath";
            this.textBoxPath.Size = new System.Drawing.Size(298, 20);
            this.textBoxPath.TabIndex = 2;
            // 
            // openFileDialogImage
            // 
            this.openFileDialogImage.Multiselect = true;
            // 
            // buttonFile
            // 
            this.buttonFile.Location = new System.Drawing.Point(6, 45);
            this.buttonFile.Name = "buttonFile";
            this.buttonFile.Size = new System.Drawing.Size(140, 23);
            this.buttonFile.TabIndex = 0;
            this.buttonFile.Text = "File";
            this.buttonFile.UseVisualStyleBackColor = true;
            this.buttonFile.Click += new System.EventHandler(this.buttonFile_Click);
            // 
            // buttonFolder
            // 
            this.buttonFolder.Location = new System.Drawing.Point(164, 45);
            this.buttonFolder.Name = "buttonFolder";
            this.buttonFolder.Size = new System.Drawing.Size(140, 23);
            this.buttonFolder.TabIndex = 0;
            this.buttonFolder.Text = "Folder";
            this.buttonFolder.UseVisualStyleBackColor = true;
            this.buttonFolder.Click += new System.EventHandler(this.buttonFolder_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripArrangeButton,
            this.toolStripButton1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.ShowItemToolTips = false;
            this.toolStrip1.Size = new System.Drawing.Size(334, 25);
            this.toolStrip1.TabIndex = 4;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripArrangeButton
            // 
            this.toolStripArrangeButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripArrangeButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripArrangeButton.Image")));
            this.toolStripArrangeButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripArrangeButton.Name = "toolStripArrangeButton";
            this.toolStripArrangeButton.Size = new System.Drawing.Size(53, 22);
            this.toolStripArrangeButton.Text = "Arrange";
            this.toolStripArrangeButton.Click += new System.EventHandler(this.toolStripArrangeButton_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(44, 22);
            this.toolStripButton1.Text = "About";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // textBoxDestination
            // 
            this.textBoxDestination.Location = new System.Drawing.Point(6, 24);
            this.textBoxDestination.Name = "textBoxDestination";
            this.textBoxDestination.Size = new System.Drawing.Size(298, 20);
            this.textBoxDestination.TabIndex = 6;
            // 
            // buttonOutput
            // 
            this.buttonOutput.Location = new System.Drawing.Point(6, 50);
            this.buttonOutput.Name = "buttonOutput";
            this.buttonOutput.Size = new System.Drawing.Size(298, 23);
            this.buttonOutput.TabIndex = 7;
            this.buttonOutput.Text = "Choose output folder";
            this.buttonOutput.UseVisualStyleBackColor = true;
            this.buttonOutput.Click += new System.EventHandler(this.buttonOutput_Click);
            // 
            // groupBoxSource
            // 
            this.groupBoxSource.Controls.Add(this.textBoxPath);
            this.groupBoxSource.Controls.Add(this.buttonFile);
            this.groupBoxSource.Controls.Add(this.buttonFolder);
            this.groupBoxSource.Location = new System.Drawing.Point(12, 25);
            this.groupBoxSource.Name = "groupBoxSource";
            this.groupBoxSource.Size = new System.Drawing.Size(310, 75);
            this.groupBoxSource.TabIndex = 8;
            this.groupBoxSource.TabStop = false;
            this.groupBoxSource.Text = "Choose source:";
            // 
            // groupBoxDestination
            // 
            this.groupBoxDestination.Controls.Add(this.textBoxDestination);
            this.groupBoxDestination.Controls.Add(this.buttonOutput);
            this.groupBoxDestination.Location = new System.Drawing.Point(12, 106);
            this.groupBoxDestination.Name = "groupBoxDestination";
            this.groupBoxDestination.Size = new System.Drawing.Size(310, 79);
            this.groupBoxDestination.TabIndex = 9;
            this.groupBoxDestination.TabStop = false;
            this.groupBoxDestination.Text = "Choose destination:";
            // 
            // checkBoxSaveDupsForTheEnd
            // 
            this.checkBoxSaveDupsForTheEnd.Location = new System.Drawing.Point(210, 188);
            this.checkBoxSaveDupsForTheEnd.Name = "checkBoxSaveDupsForTheEnd";
            this.checkBoxSaveDupsForTheEnd.Size = new System.Drawing.Size(112, 31);
            this.checkBoxSaveDupsForTheEnd.TabIndex = 10;
            this.checkBoxSaveDupsForTheEnd.Text = "Show duplicates at the end";
            this.checkBoxSaveDupsForTheEnd.UseVisualStyleBackColor = true;
            // 
            // detailedProgressBar1
            // 
            this.detailedProgressBar1.Counter = 0;
            this.detailedProgressBar1.Location = new System.Drawing.Point(12, 220);
            this.detailedProgressBar1.Name = "detailedProgressBar1";
            this.detailedProgressBar1.Size = new System.Drawing.Size(310, 27);
            this.detailedProgressBar1.TabIndex = 3;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(334, 254);
            this.Controls.Add(this.checkBoxSaveDupsForTheEnd);
            this.Controls.Add(this.groupBoxDestination);
            this.Controls.Add(this.groupBoxSource);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.detailedProgressBar1);
            this.Controls.Add(this.buttonRename);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Image Renamer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.groupBoxSource.ResumeLayout(false);
            this.groupBoxSource.PerformLayout();
            this.groupBoxDestination.ResumeLayout(false);
            this.groupBoxDestination.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonRename;
        private System.Windows.Forms.TextBox textBoxPath;
        private System.Windows.Forms.Button buttonFile;
        private System.Windows.Forms.Button buttonFolder;
        private System.Windows.Forms.OpenFileDialog openFileDialogImage;
        private DetailedProgressBar detailedProgressBar1;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialogImage;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripArrangeButton;
        private System.Windows.Forms.TextBox textBoxDestination;
        private System.Windows.Forms.Button buttonOutput;
        private System.Windows.Forms.FontDialog fontDialog1;
        private System.Windows.Forms.GroupBox groupBoxSource;
        private System.Windows.Forms.GroupBox groupBoxDestination;
        private System.Windows.Forms.CheckBox checkBoxSaveDupsForTheEnd;
    }
}

