﻿namespace FilesRenamer
{
    partial class DetailedProgressBar
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CurrentJob = new System.Windows.Forms.Label();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // CurrentJob
            // 
            this.CurrentJob.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.CurrentJob.AutoSize = true;
            this.CurrentJob.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.CurrentJob.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.CurrentJob.Location = new System.Drawing.Point(6, 7);
            this.CurrentJob.Name = "CurrentJob";
            this.CurrentJob.Size = new System.Drawing.Size(61, 13);
            this.CurrentJob.TabIndex = 1;
            this.CurrentJob.Text = "Current Job";
            this.CurrentJob.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // progressBar
            // 
            this.progressBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBar.Location = new System.Drawing.Point(0, 0);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(202, 27);
            this.progressBar.TabIndex = 2;
            // 
            // DetailedProgressBar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.CurrentJob);
            this.Controls.Add(this.progressBar);
            this.Name = "DetailedProgressBar";
            this.Size = new System.Drawing.Size(202, 27);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label CurrentJob;
        private System.Windows.Forms.ProgressBar progressBar;
    }
}
