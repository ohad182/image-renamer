﻿namespace FilesRenamer
{
    partial class ArrangeFiles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonOK = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.textBoxSuccessfulPath = new System.Windows.Forms.TextBox();
            this.labelSuccessfullPath = new System.Windows.Forms.Label();
            this.labelNote = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // buttonOK
            // 
            this.buttonOK.Location = new System.Drawing.Point(116, 66);
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(75, 23);
            this.buttonOK.TabIndex = 0;
            this.buttonOK.Text = "OK";
            this.buttonOK.UseVisualStyleBackColor = true;
            this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(197, 66);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 1;
            this.buttonCancel.Text = "Cancel";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // textBoxSuccessfulPath
            // 
            this.textBoxSuccessfulPath.Location = new System.Drawing.Point(51, 10);
            this.textBoxSuccessfulPath.Name = "textBoxSuccessfulPath";
            this.textBoxSuccessfulPath.Size = new System.Drawing.Size(221, 20);
            this.textBoxSuccessfulPath.TabIndex = 2;
            // 
            // labelSuccessfullPath
            // 
            this.labelSuccessfullPath.AutoSize = true;
            this.labelSuccessfullPath.Location = new System.Drawing.Point(13, 13);
            this.labelSuccessfullPath.Name = "labelSuccessfullPath";
            this.labelSuccessfullPath.Size = new System.Drawing.Size(32, 13);
            this.labelSuccessfullPath.TabIndex = 3;
            this.labelSuccessfullPath.Text = "Path:";
            // 
            // labelNote
            // 
            this.labelNote.AutoSize = true;
            this.labelNote.Location = new System.Drawing.Point(16, 30);
            this.labelNote.Name = "labelNote";
            this.labelNote.Size = new System.Drawing.Size(0, 13);
            this.labelNote.TabIndex = 4;
            // 
            // ArrangeFiles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 96);
            this.Controls.Add(this.labelNote);
            this.Controls.Add(this.labelSuccessfullPath);
            this.Controls.Add(this.textBoxSuccessfulPath);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ArrangeFiles";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ArrangeFiles";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonOK;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.TextBox textBoxSuccessfulPath;
        private System.Windows.Forms.Label labelSuccessfullPath;
        private System.Windows.Forms.Label labelNote;
    }
}