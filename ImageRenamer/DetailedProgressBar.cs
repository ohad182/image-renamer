﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FilesRenamer
{
    public partial class DetailedProgressBar : UserControl
    {
        public int Counter { get; set; }

        public DetailedProgressBar()
        {
            InitializeComponent();
            Reset();
        }

        public void Reset()
        {
            CurrentJob.Text = string.Empty;
            progressBar.Maximum = progressBar.Minimum = progressBar.Value = Counter = 0;
            Point pos = this.PointToScreen(CurrentJob.Location);
            pos = progressBar.PointToClient(pos);
            CurrentJob.Parent = progressBar;
            CurrentJob.Location = pos;
            CurrentJob.BackColor = Color.Transparent;
        }

        public void AddJob(string i_Item)
        {
            CurrentJob.Visible = true;
            CurrentJob.Text = string.Format("{0}: {1}", Counter++, i_Item);
        }

        public void SetMax(int i_Value)
        {
            progressBar.Maximum = i_Value;
        }

        public void JobFinished()
        {
            if (progressBar.Value < progressBar.Maximum)
            {
                progressBar.Value++;
            }
            
            if(progressBar.Value == progressBar.Maximum)
            {
                CurrentJob.Text = "Done!";
            }
        }
    }
}
