﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FilesRenamer
{
    public partial class StartingForm : Form
    {
        System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();

        public StartingForm()
        {
            InitializeComponent();
        }

        public void ShowMe()
        {
            timer.Interval = 2000;
            timer.Tick += new EventHandler(timer_tick);
            timer.Start();
            this.ShowDialog();
        }

        private void StartingForm_Load(object sender, EventArgs e)
        {
            
        }

        private void timer_tick(object sender, EventArgs e)
        {
            this.Close();
            timer.Dispose();
        }

        
    }
}
