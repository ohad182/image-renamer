﻿using ImageRenamerUtils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FilesRenamer
{
    public partial class MainForm : Form
    {
        private string[] m_Path;


        private StartingForm m_AppStart;

        public FileExist FileExistDialog { get; set; }

        public FileManager Manager { get; set; }

        public Dictionary<string,string> Duplicates { get; set; }

        public string OutputPath { get; set; }

        public string NewFullPath { get; set; }

        public string[] MyPath
        {
            get
            {
                return m_Path;
            }
            set
            {
                m_Path = value;
                if (MyPath.Length == 1)
                {
                    textBoxPath.Text = MyPath[0];
                }
                else
                {
                    textBoxPath.Text = string.Format("{0} files", MyPath.Length);
                }
            }
        }

        public MainForm()
        {
            m_AppStart = new StartingForm();
            m_AppStart.ShowMe();
            Manager = new FileManager();
            InitializeComponent();
            FileExistDialog = new FileExist();
            Duplicates = new Dictionary<string, string>();
            checkBoxSaveDupsForTheEnd.Checked = false;
        }

        private void buttonRename_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => { this.Invoke(new Action(() => RenameFiles())); });
            thread.IsBackground = true;
            thread.Start();
        }

        public void RenameFiles()
        {
            if (MyPath.Length > 0 && MyPath != null)
            {
                try
                {
                    ChangeControlsState(false);
                    detailedProgressBar1.Reset();
                    if (string.IsNullOrEmpty(OutputPath))
                    {
                        OutputPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
                    }

                    Manager.CreateAppDirectories(OutputPath);

                    detailedProgressBar1.SetMax(MyPath.Length);
                    foreach (string fileOriginPath in MyPath)
                    {
                        try
                        {
                            detailedProgressBar1.AddJob(fileOriginPath);
                            Manager.ChangeImageName(fileOriginPath, 0x9003);
                            detailedProgressBar1.JobFinished();
                        }
                        catch (FileExistException existException)
                        {
                            if (checkBoxSaveDupsForTheEnd.Checked == false)
                            {
                                ChangeExistingFileName(fileOriginPath, existException.FileName);
                            }
                            else
                            {
                                Duplicates.Add(fileOriginPath, existException.FileName);
                            }
                        }
                    }
                    if(checkBoxSaveDupsForTheEnd.Checked && Duplicates.Count > 0)
                    {
                        foreach(KeyValuePair<string,string> dicLine in Duplicates)
                        {
                            ChangeExistingFileName(dicLine.Key,dicLine.Value);
                        }
                    }
                    Manager.AppLog.Close();
                    Manager.ErrorLog.Close();

                }
                catch (Exception exception)
                {
                    Manager.ErrorLog.WriteLine("------------Exception Start------------");
                    Manager.ErrorLog.WriteLine(exception.Message);
                    Manager.ErrorLog.WriteLine(exception.StackTrace);
                    Manager.ErrorLog.WriteLine("------------Exception End------------)");
                }
                finally
                {
                    Manager.AppLog.Close();
                    Manager.ErrorLog.Close();
                    ChangeControlsState(true);
                }
            }
            else
            {
                MessageBox.Show("No files were selected.");
            }
        }

        public void ChangeExistingFileName(string i_OriginFile, string i_FileNewName)
        {
            FileExistDialog.SetForm(i_OriginFile, i_FileNewName);
            if (FileExistDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if (FileExist.Result == FileExist.eFileExistResult.KeepBoth)
                    {
                        NewFullPath = i_FileNewName.GetNumberedPath(Manager.SuccessfullDirectory);
                        File.Copy(i_OriginFile, NewFullPath);
                    }
                    else if (FileExist.Result == FileExist.eFileExistResult.LeftPhoto)
                    {
                        NewFullPath = i_FileNewName;
                        File.Delete(NewFullPath);
                        File.Copy(i_OriginFile, NewFullPath);
                    }
                    detailedProgressBar1.JobFinished();
                }
                catch (IOException exception)
                {
                    Manager.ErrorLog.WriteLine("------------Exception Start------------");
                    Manager.ErrorLog.WriteLine(exception.Message);
                    Manager.ErrorLog.WriteLine(exception.StackTrace);
                    Manager.ErrorLog.WriteLine("------------Exception End------------)");
                }
            }
        }


        public void ChangeControlsState(bool i_To)
        {
            buttonFile.Enabled = i_To;
            buttonFolder.Enabled = i_To;
            buttonRename.Enabled = i_To;
            buttonOutput.Enabled = i_To;
            checkBoxSaveDupsForTheEnd.Enabled = i_To;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void buttonFile_Click(object sender, EventArgs e)
        {
            openFileDialogImage.Filter = "Image Files (*.jpeg, *.png, *.jpg, *.gif)|*.jpeg; *.png; *.jpg; *.gif";
            if (openFileDialogImage.ShowDialog() == DialogResult.OK)
            {
                if (openFileDialogImage.FileNames.Length > 1)
                {
                    MyPath = openFileDialogImage.FileNames;
                }
                else
                {
                    MyPath = openFileDialogImage.FileNames;
                }
            }
        }

        private void buttonFolder_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialogImage.ShowDialog() == DialogResult.OK)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(folderBrowserDialogImage.SelectedPath);
                setImageFilesInMyPathFromDirectory(dirInfo);
            }
        }

        private void setImageFilesInMyPathFromDirectory(DirectoryInfo i_DirInfo)
        {
            FileInfo[] jpgFiles = i_DirInfo.GetFiles("*.jpg", SearchOption.AllDirectories);
            FileInfo[] jpegFiles = i_DirInfo.GetFiles("*.jpeg", SearchOption.AllDirectories);
            FileInfo[] bmpFiles = i_DirInfo.GetFiles("*.bmp", SearchOption.AllDirectories);
            FileInfo[] pngFiles = i_DirInfo.GetFiles("*.png", SearchOption.AllDirectories);

            int numberOfFiles = jpgFiles.Length + jpegFiles.Length + bmpFiles.Length + pngFiles.Length;
            string[] paths = new string[numberOfFiles];

            FileInfo[] files = new FileInfo[numberOfFiles];

            Array.Copy(jpgFiles, files, jpgFiles.Length);
            Array.Copy(jpegFiles, 0, files, jpgFiles.Length, jpegFiles.Length);
            Array.Copy(bmpFiles, 0, files, jpgFiles.Length + jpegFiles.Length, bmpFiles.Length);
            Array.Copy(pngFiles, 0, files, jpegFiles.Length + jpgFiles.Length + bmpFiles.Length, pngFiles.Length);

            for (int i = 0; i < numberOfFiles; i++)
            {
                paths[i] = files[i].FullName;
            }

            MyPath = paths;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            AboutBoxFilesRenamer about = new AboutBoxFilesRenamer();
            about.ShowDialog();
        }

        private void toolStripArrangeButton_Click(object sender, EventArgs e)
        {
            if (Manager.SuccessfullDirectory == null)
            {
                ArrangeFiles form = new ArrangeFiles();
                if (form.ShowDialog() == DialogResult.OK)
                {
                    Manager.SuccessfullDirectory = ArrangeFiles.SuccessPath;
                }
            }
            detailedProgressBar1.Reset();
            detailedProgressBar1.AddJob("Arranging...");
            Manager.ArrangeFiles(Manager.SuccessfullDirectory);
            detailedProgressBar1.AddJob("Done arranging");
        }

        private void buttonOutput_Click(object sender, EventArgs e)
        {
            if (folderBrowserDialogImage.ShowDialog() == DialogResult.OK)
            {
                DirectoryInfo dirInfo = new DirectoryInfo(folderBrowserDialogImage.SelectedPath);
                textBoxDestination.Text = dirInfo.FullName;
                OutputPath = dirInfo.FullName;
            }
        }
    }
}