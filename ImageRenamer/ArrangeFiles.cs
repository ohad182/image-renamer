﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FilesRenamer
{
    public partial class ArrangeFiles : Form
    {
        public static string SuccessPath { get; set; }

        public ArrangeFiles()
        {
            InitializeComponent();
            labelNote.Text = string.Format(@"** Please select the successful directory
or a directory that is filled with fixed names");
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        private void buttonOK_Click(object sender, EventArgs e)
        {
            SuccessPath = textBoxSuccessfulPath.Text;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}
