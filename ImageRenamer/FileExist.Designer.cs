﻿namespace FilesRenamer
{
    partial class FileExist
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBoxLeft = new System.Windows.Forms.PictureBox();
            this.pictureBoxRight = new System.Windows.Forms.PictureBox();
            this.buttonKeep = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonKeepLeft = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonKeepRight = new System.Windows.Forms.Button();
            this.labelInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRight)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxLeft
            // 
            this.pictureBoxLeft.InitialImage = global::ImageRenamer.Properties.Resources.empty;
            this.pictureBoxLeft.Location = new System.Drawing.Point(7, 47);
            this.pictureBoxLeft.Name = "pictureBoxLeft";
            this.pictureBoxLeft.Size = new System.Drawing.Size(100, 104);
            this.pictureBoxLeft.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxLeft.TabIndex = 0;
            this.pictureBoxLeft.TabStop = false;
            this.pictureBoxLeft.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDoubleClick);
            // 
            // pictureBoxRight
            // 
            this.pictureBoxRight.Location = new System.Drawing.Point(7, 47);
            this.pictureBoxRight.Name = "pictureBoxRight";
            this.pictureBoxRight.Size = new System.Drawing.Size(100, 104);
            this.pictureBoxRight.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBoxRight.TabIndex = 1;
            this.pictureBoxRight.TabStop = false;
            this.pictureBoxRight.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.pictureBox2_MouseDoubleClick);
            // 
            // buttonKeep
            // 
            this.buttonKeep.Location = new System.Drawing.Point(158, 170);
            this.buttonKeep.Name = "buttonKeep";
            this.buttonKeep.Size = new System.Drawing.Size(82, 24);
            this.buttonKeep.TabIndex = 2;
            this.buttonKeep.Text = "Keep Both";
            this.buttonKeep.UseVisualStyleBackColor = true;
            this.buttonKeep.Click += new System.EventHandler(this.buttonKeepBoth_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonKeepLeft);
            this.groupBox1.Controls.Add(this.pictureBoxLeft);
            this.groupBox1.Location = new System.Drawing.Point(12, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(113, 186);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBoxLeft";
            // 
            // buttonKeepLeft
            // 
            this.buttonKeepLeft.Location = new System.Drawing.Point(6, 157);
            this.buttonKeepLeft.Name = "buttonKeepLeft";
            this.buttonKeepLeft.Size = new System.Drawing.Size(99, 23);
            this.buttonKeepLeft.TabIndex = 1;
            this.buttonKeepLeft.Text = "Keep This One";
            this.buttonKeepLeft.UseVisualStyleBackColor = true;
            this.buttonKeepLeft.Click += new System.EventHandler(this.buttonKeepLeft_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonKeepRight);
            this.groupBox2.Controls.Add(this.pictureBoxRight);
            this.groupBox2.Location = new System.Drawing.Point(279, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(113, 186);
            this.groupBox2.TabIndex = 9;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "groupBoxRight";
            // 
            // buttonKeepRight
            // 
            this.buttonKeepRight.Location = new System.Drawing.Point(7, 157);
            this.buttonKeepRight.Name = "buttonKeepRight";
            this.buttonKeepRight.Size = new System.Drawing.Size(100, 23);
            this.buttonKeepRight.TabIndex = 2;
            this.buttonKeepRight.Text = "Keep This One";
            this.buttonKeepRight.UseVisualStyleBackColor = true;
            this.buttonKeepRight.Click += new System.EventHandler(this.buttonKeepRight_Click);
            // 
            // labelInfo
            // 
            this.labelInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(177)));
            this.labelInfo.Location = new System.Drawing.Point(131, 32);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(142, 67);
            this.labelInfo.TabIndex = 10;
            this.labelInfo.Text = "These files will have the same name!";
            this.labelInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FileExist
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(403, 211);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.buttonKeep);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FileExist";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FileExist";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxRight)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxLeft;
        private System.Windows.Forms.PictureBox pictureBoxRight;
        private System.Windows.Forms.Button buttonKeep;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonKeepLeft;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonKeepRight;
        private System.Windows.Forms.Label labelInfo;
    }
}