﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("ImageRenamer")]
[assembly: AssemblyDescription("This app will help you organize your photos.         The application copies the images to a new folder where the pictures will be renamed by the shooting date / creation of the photo.                                After using the app, A folder will be created with new images, you can click on the button 'Arrange' to let the app arrange the images in the same place with new folders by date.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Developed and Designed by Ohad Cohen")]
[assembly: AssemblyProduct("ImageRenamer")]
[assembly: AssemblyCopyright("Copyright © Ohad Cohen 2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8dfa3c1b-8af4-4b59-9736-b8ed84730f78")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
