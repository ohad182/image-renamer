﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FilesRenamer
{
    public partial class FileExist : Form
    {
        public string ImageFrom { get; set; }

        public string ImageTo { get; set; }

        public static eFileExistResult Result { get; set; }

        public enum eFileExistResult
        {
            LeftPhoto = 1,
            RightPhoto,
            KeepBoth
        }
        public FileExist()
        {
            InitializeComponent();
            ToolTip tip = new ToolTip();
            tip.ToolTipIcon = ToolTipIcon.Info;
            tip.IsBalloon = true;
            tip.ShowAlways = true;
            tip.SetToolTip(pictureBoxLeft, ImageFrom);
            ToolTip top = new ToolTip();
            top.ToolTipIcon = ToolTipIcon.Info;
            top.IsBalloon = true;
            top.ShowAlways = true;
            top.SetToolTip(pictureBoxRight, ImageTo);
        }

        public void SetForm(string i_ImagePath1, string i_ImagePath2)
        {
            groupBox1.Text = Path.GetFileName(i_ImagePath1);
            groupBox2.Text = Path.GetFileName(i_ImagePath2);
            ImageFrom = i_ImagePath1;
            ImageTo = i_ImagePath2;
            pictureBoxLeft.LoadAsync(i_ImagePath1);
            pictureBoxRight.LoadAsync(i_ImagePath2);
        }

        private void buttonKeepBoth_Click(object sender, EventArgs e)
        {
            Result = eFileExistResult.KeepBoth;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void pictureBox2_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start(ImageTo);
        }

        private void pictureBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            System.Diagnostics.Process.Start(ImageFrom);
        }

        private void buttonKeepLeft_Click(object sender, EventArgs e)
        {
            Result = eFileExistResult.LeftPhoto;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonKeepRight_Click(object sender, EventArgs e)
        {
            Result = eFileExistResult.RightPhoto;
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
    }
}